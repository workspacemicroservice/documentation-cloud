CONCEPTOS PREVIOS:
* PCF: Pivotal Cloud Foundry, es la nube a la cual, en este caso, nos conectaremos.
* cf CLI: Cloud Foundry Command Line Interface, es el cliente que utilizamos para realizar comandos y conectarnos a la nube Pivotal Cloud Foundry.
* org: Proyecto que posee un conjunto de spaces. Se puede observar facilmente en Pivotal Cloud Foundry web.
* spaces: Son los espacios o agrupaciones que pueden contener a las aplicaciones (app). Se puede observar facilmente en Pivotal Cloud Foundry web.
* app: Son las aplicaciones que poseen todo el código fuente a desplegar.

COMANDOS CLOUD FOUNDRY:
- cf login  =>  Comando para loguearte mediante el cliente (Cloud Foundry CLI) a la nube en el que se le asigneen API (en nuestro caso Pivotal CF).

- cf help   =>  Comando para obtener ayuda del cliente.
- cf delete -h   =>  Comando para obtener ayuda más específica dependiendo al inicio del comando, esto se logra gracias al flag -h.
- cf config --locale LENGUAGE   => Comando para cambiar la configuracion del lenguaje del cliente (ejm. de LENGUAGE: es-ES, en-US, etc).
- cf set-org-role huey@example.com example-org OrgManager    => Comando para asignar roles a un usuario en un proyecto con el rol "OrgManager", 
                                                                aunque posee variaciones con respecto al comando "set-org-role" para realizar desasignaciones y con respecto a espacios. 
                                                                Para más información ver (3*).
- cf push APP-NAME  =>  Comando para subir una app asignandole un nombre (ejm. de APP-NAME: servicio-config-server), siempre y cuando te encuentres en el directorio donde esta la app.
- cf push APP-NAME -f manifest.yml => Comando para subir una app teniendo opciones del comando los que se encuentran en el manifest.yml.
- cf push APP-NAME --hostname APP-HOSTNAME  => Comando para subir una app y especificar el Hostname que deseamos asignarle (ejm. de APP-HOSTNAME: test1-config-server) (DEPRECATED).
- cf push APP-NAME -n APP-HOSTNAME  => Comando para subir una app y especificar el Hostname que deseamos asignarle (ejm. de APP-HOSTNAME: test1-config-server).
- cf push APP-NAME --random-route  => Comando para subir una app y generar un Hostname que incluya el nombre de la app y palabras random (ejemplo: servicio-config-server-kind-mouse-ia).
- cf push -m 128M   =>  Comando para subir una app y especificar el espacio de memoria (64M, 128M, 256M, 512M, 1G, ó 2G).

- cf app APP-NAME   => Comando para visualizar el espacio de memoria utilizado por una app (ejm. de APP-NAME: servicio-config-server).
- cf logs APP-NAME  =>  Comando para visualizar el log de actividad de la app APP-NAME mientras se despliega, inicie una nueva ventana de terminal.

RUTAS:
(1* Getting Started Deploying Spring Apps) https://docs.run.pivotal.io/buildpacks/java/getting-started-deploying-apps/gsg-spring.html
(2* Installing the cf CLI) https://docs.run.pivotal.io/cf-cli/install-go-cli.html
(3* Getting Started with the cf CLI) https://docs.run.pivotal.io/cf-cli/getting-started.html
(4* Pivotal Cloud Foundry Tutorial - Deploy Spring Boot Application Hello World Example) https://www.javainuse.com/pcf/pcf-hello